#!/bin/sh

echo -n "$KAFKA_CA_AUTHORITY" > /certs/kafka-ca.crt &&
/wait

if [[ ! -z "${RUN_MIGRATIONS}" ]]; then
  php artisan migrate --force
fi

php artisan $CLI_TO_RUN >> /dev/stdout 2>&1
