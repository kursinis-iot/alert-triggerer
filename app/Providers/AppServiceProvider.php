<?php

namespace App\Providers;

use Illuminate\Support\ServiceProvider;
use Kudze\KafkaConsumerProducer\Services\KafkaProducer;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register(): void
    {
        $this->app->singleton(KafkaProducer::class, function () {
            $producer = new KafkaProducer();
            $producer->init();

            return $producer;
        });
    }
}
