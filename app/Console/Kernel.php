<?php

namespace App\Console;

use App\Console\Commands\ConsumeCreatedDevices;
use App\Console\Commands\ConsumeDeviceSensorAlertTopic;
use App\Console\Commands\ConsumeDeviceSensorLog;
use App\Console\Commands\TriggerDowntimeAlerts;
use Illuminate\Console\Scheduling\Schedule;
use Laravel\Lumen\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        ConsumeCreatedDevices::class,
        ConsumeDeviceSensorAlertTopic::class,
        ConsumeDeviceSensorLog::class,
        TriggerDowntimeAlerts::class
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        $schedule->command('trigger:downtime:alerts')->everyMinute();
    }
}
