<?php

namespace App\Console\Commands;

use App\Console\Commands\Traits\DeviceAlertFactoryTrait;
use App\Models\Device;
use Kudze\LumenKafkaConsumerProducer\Command\AbstractConsumerJsonCommand;
use RdKafka\Message;

class ConsumeCreatedDevices extends AbstractConsumerJsonCommand
{
    use DeviceAlertFactoryTrait;

    protected $signature = "consume:device:created";
    protected $description = "Consumes device created topic";

    protected function getKafkaTopics(): array
    {
        return [env('KAFKA_DEVICE_ADDITION_TOPIC')];
    }

    protected function processJsonMessage(Message $message, array $payload): void
    {
        $this->io->writeln("Offset: $message->offset, received device created message!");

        //Create device
        /** @var Device $device */
        $device = Device::query()->create([
            'uuid' => $message->key,
            'title' => $payload['device_title']
        ]);

        //Create sensors.
        foreach ($payload['sensors'] as $sensorData) {
            $sensorUuid = $sensorData['uuid'];

            $device->sensors()->create([
                'uuid' => $sensorUuid,
                'title' => $sensorData['title'],
                'jsonpath_query' => $sensorData['jsonpath_query']
            ]);

            //Create alerts
            $sensorAlerts = array_filter($payload['alerts'], function (array $alert) use ($sensorUuid) {
                return $alert['device_sensor_uuid'] === $sensorUuid;
            });

            $this->createAlertsForSensor($sensorAlerts, $payload['emails'], $sensorUuid);
        }
    }
}
