<?php

namespace App\Console\Commands;

use App\Console\Commands\Traits\DeviceAlertFactoryTrait;
use App\Models\DeviceSensorAlert;
use Kudze\LumenKafkaConsumerProducer\Command\AbstractConsumerJsonCommand;
use RdKafka\Message;

class ConsumeDeviceSensorAlertTopic extends AbstractConsumerJsonCommand
{
    use DeviceAlertFactoryTrait;

    protected $signature = "consume:device:sensor:alert";
    protected $description = "Consumes device sensor alert topic";

    protected function getKafkaTopics(): array
    {
        return [env('KAFKA_DEVICE_ALERTS_TOPIC')];
    }

    protected function processJsonMessage(Message $message, array $payload): void
    {
        $this->io->writeln("Offset: $message->offset, received device alert modified!");

        //First of all we delete all the device alerts.
        [
            'sensor_uuid' => $sensorUuid,
            'alerts' => $alerts,
            'emails' => $emails
        ] = $payload;
        DeviceSensorAlert::query()->where('device_sensor_uuid', $sensorUuid)->delete();

        //And then create.
        $this->createAlertsForSensor($alerts, $emails, $sensorUuid);
    }
}
