<?php

namespace App\Console\Commands\Traits;

use App\Models\DeviceSensorAlert;

trait DeviceAlertFactoryTrait
{

    /**
     * Also creates email recipients.
     *
     * @param array $alerts
     * @param array $emails
     * @return void
     */
    protected function createAlertsForSensor(array $alerts, array $emails, string $sensorUuid): void
    {
        foreach ($alerts as $alertData) {
            $alertUuid = $alertData['uuid'];

            /** @var DeviceSensorAlert $alert */
            $alert = DeviceSensorAlert::query()->create([
                'uuid' => $alertUuid,
                'device_sensor_uuid' => $sensorUuid,
                'title' => $alertData['title'],
                'interval' => $alertData['interval'],
                'type' => $alertData['type'],
                'payload' => $alertData['payload']
            ]);

            //Create emails
            $alertEmails = array_filter($emails, function (array $email) use ($alertUuid) {
                return $email['device_sensor_alert_uuid'] === $alertUuid;
            });

            foreach ($alertEmails as $emailData)
                $alert->emailRecipients()->create([
                    'email' => $emailData['email']
                ]);
        }
    }

}
