<?php

namespace App\Console\Commands;

use App\Models\DeviceSensorAlert;
use App\Service\AlertEmitterService;
use App\Service\AlertHandlers\AlertHandlerService;
use Carbon\Carbon;
use Kudze\LumenBaseCli\Command\AbstractCommand;
use RdKafka\Exception as KException;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

class TriggerDowntimeAlerts extends AbstractCommand
{
    protected $signature = "trigger:downtime:alerts";
    protected $description = "Triggers downtime alerts";

    protected AlertEmitterService $alertEmitterService;

    public function __construct(AlertEmitterService $alertEmitterService)
    {
        $this->alertEmitterService = $alertEmitterService;

        parent::__construct();
    }

    /**
     * @throws KException
     */
    public function execute(InputInterface $input, OutputInterface $output): int
    {
        $this->io->info("Fetching downtime alerts to trigger!");
        $toTrigger = DeviceSensorAlert::query()
            ->where('type', AlertHandlerService::ALERT_TYPE_DOWNTIME)
            ->whereNotNull('alert_memory')
            ->whereRaw(DeviceSensorAlert::WHERE_RAW_UNEXPIRED)
            ->whereRaw("NOW() > TIMESTAMPADD(SECOND, payload, alert_memory)")
            ->get();

        if($toTrigger->isEmpty())
        {
            $this->io->success("There were no downtime alerts to trigger!");
            return self::SUCCESS;
        }

        $count = $toTrigger->count();
        $this->io->info("Triggering $count downtime alerts...");

        //TODO: chunk result?
        /** @var DeviceSensorAlert $alert */
        foreach ($toTrigger as $alert) {
            $lastUpdatedAt = $alert->getAttribute('alert_memory');
            $interval = $alert->getAttribute('payload');

            $this->alertEmitterService->failAlert(
                $alert,
                null,
                Carbon::now('UTC')->getPreciseTimestamp(3),
                "Sensor data was last received at $lastUpdatedAt, which is more than $interval seconds before now.",
                ['alert_memory' => null]
            );
        }

        $this->io->success("Finished");
        return self::SUCCESS;
    }
}
