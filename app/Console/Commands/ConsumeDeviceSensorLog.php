<?php

namespace App\Console\Commands;

use App\Service\DeviceSensorLogSubscriber;
use Kudze\KafkaConsumerProducer\Services\KafkaConsumer;
use Kudze\LumenKafkaConsumerProducer\Command\AbstractConsumerJsonCommand;
use RdKafka\Message;
use Throwable;

class ConsumeDeviceSensorLog extends AbstractConsumerJsonCommand
{
    protected $signature = "consume:device:sensor:log";
    protected $description = "Consumes device sensor log topic";

    public function __construct(
        KafkaConsumer                       $consumer,
        protected DeviceSensorLogSubscriber $sensorLogSubscriber)
    {
        parent::__construct($consumer);
    }

    protected function getKafkaTopics(): array
    {
        return [env('KAFKA_DEVICE_SENSOR_TOPIC')];
    }

    protected function processJsonMessage(Message $message, array $payload): void
    {
        $this->io->writeln("Offset: $message->offset, received device sensor log!");

        foreach ($payload as $sensorUuid => $sensorValue)
            try {
                $this->sensorLogSubscriber->handleSensorLog($sensorUuid, $sensorValue, $message->timestamp);
            } catch (Throwable $throwable) {
                $this->io->warning($throwable->getMessage());
            }
    }
}
