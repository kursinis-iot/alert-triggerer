<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use Illuminate\Database\Eloquent\Relations\HasMany;

class Device extends Model
{
    use HasUuids;

    protected $table = "device";
    protected $primaryKey = "uuid";
    public $incrementing = false;
    protected $keyType = "string";

    public function sensors(): HasMany
    {
        return $this->hasMany(DeviceSensor::class, 'device_uuid', 'uuid');
    }

    protected $fillable = [
        'uuid',
        'title',
        'created_at',
        'updated_at'
    ];
}
