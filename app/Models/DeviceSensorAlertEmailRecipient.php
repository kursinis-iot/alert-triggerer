<?php

namespace App\Models;

use App\Models\Traits\HasUuidsComposite;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;

class DeviceSensorAlertEmailRecipient extends Model
{
    use HasUuidsComposite;

    protected $table = 'device_sensor_alert_email_recipient';
    protected $primaryKey = ["device_sensor_alert_uuid", "email"];
    public $incrementing = false;
    protected $keyType = "string";

    public function alert(): BelongsTo {
        return $this->belongsTo(DeviceSensorAlert::class, 'device_sensor_alert_uuid', 'uuid');
    }

    protected $fillable = [
        'device_sensor_alert_uuid',
        'email',
        'created_at',
        'updated_at'
    ];
}