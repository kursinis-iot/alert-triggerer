<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class DeviceSensor extends Model
{
    use HasUuids;

    protected $table = "device_sensor";
    protected $primaryKey = "uuid";
    public $incrementing = false;
    protected $keyType = "string";

    public function device(): BelongsTo
    {
        return $this->belongsTo(Device::class, 'device_uuid', 'uuid');
    }

    public function alerts(): HasMany
    {
        return $this->hasMany(DeviceSensorAlert::class, 'device_sensor_uuid', 'uuid');
    }

    protected $fillable = [
        'uuid',
        'title',
        'device_uuid',
        'jsonpath_query',
        'created_at',
        'updated_at',
    ];
}
