<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Concerns\HasUuids;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsTo;
use Illuminate\Database\Eloquent\Relations\HasMany;

class DeviceSensorAlert extends Model
{
    public const WHERE_RAW_UNEXPIRED = "last_triggered_at IS NULL OR TIMESTAMPADD(SECOND, `interval`, last_triggered_at) < NOW()";

    use HasUuids;

    protected $table = 'device_sensor_alert';
    protected $primaryKey = 'uuid';
    public $incrementing = false;
    protected $keyType = "string";

    public function sensor(): BelongsTo {
        return $this->belongsTo(DeviceSensor::class, 'device_sensor_uuid', 'uuid');
    }

    public function emailRecipients(): HasMany {
        return $this->hasMany(DeviceSensorAlertEmailRecipient::class, 'device_sensor_alert_uuid', 'uuid');
    }

    protected $fillable = [
        'uuid',
        'device_sensor_uuid',
        'title',
        'interval',
        'type',
        'payload',
        'last_triggered_at',
        'alert_memory',
        'created_at',
        'updated_at'
    ];

    protected $casts = [
        'payload' => 'array',
        'alert_memory' => 'array'
    ];
}
