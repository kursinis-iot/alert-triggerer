<?php

namespace App\Service;

use App\Models\DeviceSensorAlert;
use Carbon\Carbon;
use Kudze\KafkaConsumerProducer\Services\KafkaProducer;
use RdKafka\Exception as KException;

class AlertEmitterService
{
    protected KafkaProducer $kafkaProducer;

    public function __construct(KafkaProducer $kafkaProducer)
    {
        $this->kafkaProducer = $kafkaProducer;
    }

    /**
     * @throws KException
     */
    public function failAlert(DeviceSensorAlert $alert, ?array $sensorValue = null, ?int $timestamp = null, ?string $message = null, array $updates = []): void
    {
        $sensor = $alert->sensor()->with('device')->firstOrFail();
        $emailRecipients = $alert->emailRecipients()->pluck('email');

        foreach ($emailRecipients as $email) {
            $payload = json_encode([
                'from' => env('MAILER_FROM'),
                'to' => $email,
                'template' => 'ALERT_TRIGGER',
                'payload' => [
                    'sensor' => $sensor->toArray(),
                    'alert' => $alert->toArray(),
                    'sensorValue' => $sensorValue,
                    'timestamp' => $timestamp,
                    'message' => $message
                ],
            ]);

            $this->kafkaProducer->produce($payload, env('KAFKA_MAIL_TOPIC'));
        }
        $this->kafkaProducer->flush(1000);

        $updates['last_triggered_at'] = Carbon::now('UTC')->toDateTimeString();
        $alert->update($updates);
    }

}
