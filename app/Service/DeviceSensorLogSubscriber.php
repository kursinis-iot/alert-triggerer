<?php

namespace App\Service;

use App\Models\DeviceSensorAlert;
use App\Service\AlertHandlers\AlertHandlerService;
use Illuminate\Database\Eloquent\Collection;
use InvalidArgumentException;

class DeviceSensorLogSubscriber
{
    protected AlertHandlerService $alertHandlerService;

    public function __construct(AlertHandlerService $alertHandlerService)
    {
        $this->alertHandlerService = $alertHandlerService;
    }

    public function handleSensorLog(string $sensorUuid, mixed $sensorValue, int $timestamp): void
    {
        $alerts = $this->loadAlertsToAnalyse($sensorUuid);
        if ($alerts->isEmpty())
            return;

        /** @var DeviceSensorAlert $alert */
        foreach ($alerts as $alert) {
            $alertType = $alert->getAttribute('type');
            $alertHandler = $this->alertHandlerService->getHandlerByAlertType($alertType);

            $alertHandler->handle($alert, $sensorValue, $timestamp);
        }
    }

    protected function loadAlertsToAnalyse(string $sensorUuid): Collection
    {
        return DeviceSensorAlert::query()->where('device_sensor_uuid', $sensorUuid)->whereRaw(DeviceSensorAlert::WHERE_RAW_UNEXPIRED)->get();
    }

}
