<?php

namespace App\Service\AlertHandlers;

use App\Models\DeviceSensorAlert;
use Carbon\Carbon;

class DowntimeAlertHandler extends AbstractAlertHandler
{
    public function handle(DeviceSensorAlert $alert, mixed $sensorValue, int $timestamp)
    {
        $alert->update(['alert_memory' => Carbon::now('UTC')->toDateTimeString()]);
    }
}
