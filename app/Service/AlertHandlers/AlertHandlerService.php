<?php

namespace App\Service\AlertHandlers;

use InvalidArgumentException;

class AlertHandlerService
{
    const ALERT_TYPE_RANGE = "range";
    const ALERT_TYPE_DOWNTIME = "downtime";

    public function getHandlerByAlertType(string $alertType): AbstractAlertHandler
    {
        return match ($alertType) {
            self::ALERT_TYPE_RANGE => app(RangeAlertHandler::class),
            self::ALERT_TYPE_DOWNTIME => app(DowntimeAlertHandler::class),
            default => throw new InvalidArgumentException("Invalid alert type: $alertType, provided to getHandlerByAlertType"),
        };
    }
}
