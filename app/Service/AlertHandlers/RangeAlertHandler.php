<?php

namespace App\Service\AlertHandlers;

use App\Models\DeviceSensorAlert;
use RdKafka\Exception as KException;

class RangeAlertHandler extends AbstractAlertHandler
{

    /**
     * @throws KException
     */
    public function handle(DeviceSensorAlert $alert, mixed $sensorValue, int $timestamp): void
    {
        $payload = $alert->getAttribute('payload');

        $min = $payload['min'];
        $max = $payload['max'];

        $message = null;
        $this->traverseSensorValue($sensorValue, function (mixed $value) use (&$message, $min, $max) {
            $message = $this->checkValue($value, $min, $max);
            return is_null($message);
        });

        if ($message === null)
            return;

        $this->failAlert($alert, $sensorValue, $timestamp, $message);
    }

    protected function checkValue(int|string|float|null $sensorValue, float $min, float $max): ?string
    {
        if ($sensorValue === null)
            return null;

        if ($sensorValue > $max)
            return "Sensor value: $sensorValue has exceeded max bound: $max.";

        if ($sensorValue < $min)
            return "Sensor value: $sensorValue has fallen behind min bound: $min.";

        return null;
    }
}
