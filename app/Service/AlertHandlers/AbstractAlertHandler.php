<?php

namespace App\Service\AlertHandlers;

use App\Models\DeviceSensorAlert;
use App\Service\AlertEmitterService;
use RdKafka\Exception as KException;

abstract class AbstractAlertHandler
{
    protected AlertEmitterService $alertEmitterService;

    public function __construct(AlertEmitterService $alertEmitterService)
    {
        $this->alertEmitterService = $alertEmitterService;
    }

    public abstract function handle(DeviceSensorAlert $alert, mixed $sensorValue, int $timestamp);

    /**
     * @throws KException
     */
    protected function failAlert(DeviceSensorAlert $alert, array $sensorValue, int $timestamp, ?string $message = null): void
    {
        $this->alertEmitterService->failAlert($alert, $sensorValue, $timestamp, $message);
    }

    /**
     * @param mixed $sensorValue
     * @param callable $callback - function ($value): bool, if returns false traverse will be stopped. $value is a scalar or null.
     * @return bool - internal
     */
    protected function traverseSensorValue(mixed $sensorValue, callable $callback): bool
    {
        if (is_scalar($sensorValue) || is_null($sensorValue))
            return $callback($sensorValue);

        foreach ($sensorValue as $value)
            if (!$this->traverseSensorValue($value, $callback))
                return false;

        return true;
    }
}
