<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     */
    public function up(): void
    {
        Schema::create('device', function (Blueprint $table) {
            $table->uuid()->primary();
            $table->timestamps();

            $table->string("title");
        });

        Schema::create('device_sensor', function (Blueprint $table) {
            $table->uuid()->primary();
            $table->timestamps();

            $table->string("title");
            $table->foreignUuid('device_uuid')->constrained('device', 'uuid')->cascadeOnDelete();
            $table->string('jsonpath_query');
        });

        Schema::create('device_sensor_alert', function (Blueprint $table) {
            $table->uuid()->primary();
            $table->timestamps();

            $table->foreignUuid('device_sensor_uuid')->constrained('device_sensor', 'uuid')->cascadeOnDelete();
            $table->string('title');
            $table->integer("interval");
            $table->string('type');
            $table->json('payload');
            $table->timestamp('last_triggered_at')->nullable()->index();
            $table->json('alert_memory')->nullable(); //TODO: if we are replicating this service, move this to redis.
        });

        Schema::create('device_sensor_alert_email_recipient', function (Blueprint $table) {
            $table->uuid('device_sensor_alert_uuid');
            $table->foreign('device_sensor_alert_uuid', 'device_sensor_alert_uuid_email_fk')->on('device_sensor_alert')->references('uuid')->cascadeOnDelete();
            $table->string('email', 255);
            $table->timestamps();

            $table->primary(['device_sensor_alert_uuid', 'email']);
        });
    }

    /**
     * Reverse the migrations.
     */
    public function down(): void
    {
        Schema::dropIfExists('device_sensor_alert_email_recipient');
        Schema::dropIfExists('device_sensor_alert');
        Schema::dropIfExists('device_sensor');
        Schema::dropIfExists('device');
    }
};
